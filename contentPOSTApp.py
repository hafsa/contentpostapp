from webAPP import WebApp

class ContentPOST(WebApp):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.content_dict = {
            '/': '<html><body><h1>Main page</h1></body></html>',
            }

    def process(self, analyzed):
        recurso = analyzed['recurso']
        metodo = analyzed['metodo']

        formulario = '<form method="post">' \
                     '<label>Introduce el recurso para guardar:' \
                     '<input name="name" autocomplete="name"/>' \
                     '</label>' \
                     '<button>Enviar</button>' \
                     '</form>'

        response = recurso +" "+ formulario

        if metodo == 'GET':
            if recurso in self.content_dict:
                http_code = "200 OK"
                html_content = "<html><body>La p&aacute;gina ya hab&iacute;a sido solicitada " \
                               "y guardada anteriormente"+response+ "<h1></h1></body></html>"
            else:
                http_code = "200 OK"
                html_content = "<html><body><h1>p&aacute;gina nueva:"+response+ "</h1></body></html>"
                self.content_dict[recurso] = recurso
            return http_code, html_content

        elif metodo == 'POST':
            http_code = "200 OK"
            self.content_dict[recurso] = recurso
            html_content = "<html><body><h1>p&aacute;gina:"+response+ "</h1></body></html>"

            return http_code, html_content


if __name__ == '__main__':
    web_app = ContentPOST('', 1234)
    web_app.accept_clients()


